import {filter} from 'unist-util-filter'
import fs from 'node:fs/promises'
import { Gitlab } from '@gitbeaker/node'
import {map} from 'unist-util-map'
import process from 'node:process'
import { remark } from 'remark'
import yaml from 'js-yaml'

async function handlerProject(project, path) {
  const content = await getDocumentation(project)
  if (content === '' ) {
    return
  }

  const parsedContent = content.replace(/^#/m, '<!--more-->\n#')
  if (parsedContent === '') {
    return
  }

  const meta = await getMetaData(project)
  if (meta.base === '') {
    return
  }

  const data = yaml.dump({
    title: project.name,
    anchor: project.name,
    link: project.web_url,
    dockerimage: meta.base,
  })

  await fs.writeFile(`${path.replace(/\/$/, '')}/${project.name}.md`,
    `---\n${data}---\n${parsedContent}`)
}

async function getDocumentation(project) {
  let keep = false
  let contents = ''
  try {
    contents = await api.RepositoryFiles.showRaw(
      project.id,
      'README.md',
      project.default_branch,
    )
  } catch (error) {
    return ''
  }

  const tree = remark().parse(contents)

  let newTree = filter(tree, (node) => {
    if (node.type === 'root') {
      return true
    }

    if (node.type === 'definition') {
      return true
    }

    if (node.type === 'heading' && node.depth === 2) {
      if (
        node.children[0].value === 'Usage'
      ) {
        keep = true
        return false
      }
      if (
        node.children[0].value === 'Examples'
        || node.children[0].value === 'Example'
      ) {
        keep = true
        return true
      }
      keep = false
    }
    return keep
  })

  newTree = map(newTree, (node) => {
    if (node.type === 'heading' && node.depth == 2) {
      // node.depth = 4;
    }
    return node
  })

  return remark().stringify(newTree)
}

async function getMetaData(project) {
  return {
    base: await api.RepositoryFiles.showRaw(
      project.id,
      'Dockerfile',
      project.default_branch,
    ).then((content) =>
      content
        .split('\n')
        .filter((line) => line.match(/^FROM/))
        .pop()
        .split(' ')
        .pop()
        .split('@')
        .shift(),
    ).catch(() => ''),
  }
}

const args = process.argv.slice(2)

function errorMessage(message){
  return `\x1b[41m\x1b[37m Error: \x1b[0m ${message}\n`
}

if (args.length < 2 || args.length > 3) {
  process.stderr.write(errorMessage('Two parameters required: \x1b[1m<group-number> <path> \x1b[2m[host]\x1b[0m\n'))
  process.exit(9)
}

const groupNumber = args[0]
const path = args[1]
const host = args[2] || 'http://gitlab.com'

fs.access(path)
  .catch((error) => {
    process.stderr.write(`${errorMessage(`Can not read given path '${path}'`)}\t \x1b[2m(${error})\x1b[0m\n`)
    process.exit(9)
  })

const api = new Gitlab({
  host: host,
})

const projects = api.Groups.projects(groupNumber)

projects.then(projectWalker)

function projectWalker(projects) {
  projects.map((project) => {
    // skip the docker-update project, it is not a real project
    if (project.name != 'docker-update') {
      console.dir(project.name)
      handlerProject(project, path)
    }
  })
}

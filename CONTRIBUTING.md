# Contributing

Thank you for being involved! 😍

This is an active open-source project.  We are always open to people who want to use or contribute to it.

Questions can be asked (or feedback given) by opening an issues on Gitlab or by joining [the pipeline-component discord channel][discord-join]

We accept contributions via merge-request on Gitlab.

Any contribution is welcome, and you will be given full credit for your efforts.

<!-- toc -->

## Reporting issues

To ensure your privacy and security, please do not include passwords or personally identifiable information in your bug report and sample code.

Some general guidelines:

- Before opening a new issue, please use the Gitlab search feature to see if the issue has already been reported

- If your issue is not specific to one repository but more about things in general, open an issue in the [organisation issues repository][issues-org]

- If your issue is about the website or generic documentation, open an issue at [the website repository][issues-website]


When reporting issues, please try to be as descriptive as possible, and include as much relevant information as you can to help us reproduce the issue.

A good bug report includes:

- Expected outcome
- Actual outcome
- Steps to reproduce (including any relevant sample code)
- Any other information that will help us debug and reproduce the issue (error messages, stack traces, system/environment information, screenshots, etc.)

A step-by-step guide on how to reproduce the issue will greatly increase the chances of your issue being resolved in a timely manner.

If your issue involves installing, updating or resolving dependencies, the chance of us being able to reproduce your issue will be much higher if you share the file defining your dependency with us.

Due to time constraints, we are not always able to respond as quickly as we would like. Please do not take delays personally and feel free to remind us if you feel that we forgot to respond!

## Development

Small changes can be made directly in the Gitlab web interface.

Larger changes require a local clone of the repository.

Both cases require you to have a Gitlab account, as a fork of the repository is needed.

### Overview

The overall process for contributing is:

1. Fork this repository
2. Make changes in the forked repository
3. Create a merge request in this repository for the changes in the forked repository (bonus points for topic branches!)
4. Address any feedback given on the merge request
5. ...
6. Profit!

Once the merge request is accepted, the website is automatically built and deployed.

For local development, some additional steps are needed:

- A local clone of the forked repository needs to be created before changes are made
- The website needs to be built and tested locally before committing changes
- The committed changes need to be pushed to the forked repository before a merge request can be created

### Local development

#### Prerequisites

To create a local clone of the repository, [`git` needs to be installed][git-install].


To build to the website, [Hugo needs to be installed][hugo-install] or run in [the Docker container available from Gitlab][gitlab-docker-hugo].

Before making commits, please make sure [your user name and email address have been set up for Git][git-config], to avoid `silly nick name <root@localhost>` in the commit history of the project.

[discord-join]: https://discord.gg/vhxWFfP
[git-config]: http://git-scm.com/book/en/v2/Getting-Started-First-Time-Git-Setup
[git-install]: https://git-scm.com/book/en/v2/Getting-Started-Installing-Git
[gitlab-docker-hugo]: https://gitlab.com/pages/hugo/container_registry
[hugo-install]: https://gohugo.io/installation/
[issues-org]: https://gitlab.com/pipeline-components/org/issues/-/issues
[issues-website]: https://gitlab.com/pipeline-components/org/pipeline-components.dev/-/issues

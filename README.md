# The `pipeline-components.dev` website

![Project Stage][project-stage-shield]
[![GitLab CI][gitlabci-shield]][gitlabci]
![Project Maintenance][maintenance-shield]
[![License][license-shield]][license-page]

[![standard-readme compliant][standard-readme-shield]][standard-readme-link]

This repository contains the templates, contents and configuration needed to create [the website for the Pipeline-Components project][pipeline-components-site].

The website is built using [Hugo][hugo-site] and is hosted on [GitLab Pages][gitlab-pages].

## Install

Installation is not needed, just visit https://pipeline-components.dev/ to see the website.

## Usage

The website contains three major sections:

- A landing page
- A list of containers, and container details
- Examples of how to use containers on various platforms

## Contributing

This is an active open-source project. We are always open to people who want to use the code or contribute to it.

Issues can be reported using [Gitlab issues][issues] or by joining [the pipeline-component discord channel][discord-join]

For further information, please read the [contributing guidelines](./CONTRIBUTING.md).

Thank you for being involved! 😍

## License

This project is licensed under the [MIT License](./LICENSE) by [Robbert Müller][mjrider].

[discord-join]: https://discord.gg/vhxWFfP
[gitlab-pages]: https://docs.gitlab.com/ee/user/project/pages/
[gitlabci-shield]: https://gitlab.com/pipeline-components/org/pipeline-components.dev/badges/master/pipeline.svg
[gitlabci]: https://gitlab.com/pipeline-components/_template_/-/commits/master
[issues]: https://gitlab.com/pipeline-components/org/pipeline-components.dev/issues
[hugo-site]: https://gohugo.io/
[issues]: https://gitlab.com/pipeline-components/org/pipeline-components.dev/issues
[license-page]: ./LICENSE
[license-shield]: https://img.shields.io/badge/License-MIT-green.svg
[maintenance-shield]: https://img.shields.io/maintenance/yes/2024.svg
[mjrider]: https://gitlab.com/mjrider
[pipeline-components-site]: https://pipeline-components.dev
[project-stage-shield]: https://img.shields.io/badge/project%20stage-production%20ready-brightgreen.svg
[standard-readme-link]: https://github.com/RichardLitt/standard-readme
[standard-readme-shield]: https://img.shields.io/badge/-Standard%20Readme-brightgreen.svg

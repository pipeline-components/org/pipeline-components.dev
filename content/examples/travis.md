---
title: "Travis"
anchor: "travis"
---

While pipeline components are build with gitlab in mind, running in travis is also possible.
<!--more-->
```yaml
---
language: shell
services:
  - docker

stages:
  - lint
  - security

before_install:
  - docker info

jobs:
  include:
    - stage: lint
      script:
        - docker run -v $(pwd):/code pipelinecomponents/php-linter:latest parallel-lint --colors .
    - script:
        - docker run -v $(pwd):/code pipelinecomponents/stylelint:latest stylelint --color '**/*.css'
    - script:
        - docker run -v $(pwd):/code pipelinecomponents/php-codesniffer:latest phpcs -s -p --colors --extensions=php --standard=PSR12 .
    - stage: security
      script:
        - docker run -v $(pwd):/code pipelinecomponents/php-security-checker:latest security-checker security:check composer.lock
```

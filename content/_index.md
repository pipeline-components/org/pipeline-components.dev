---
title: Pipeline Components
---
## What is this

A project that builds small and fast, constantly updated containers for CI/CD purposes.
In doing so we hope to help developers to focus on their projects instead on building the tools they need for the project.

While the project started as a gitlab only project we are not looking on building gitlab only solutions.

## Support

Got questions?

Check out the [discord channel][discord]

[discord]: https://discord.gg/vhxWFfP

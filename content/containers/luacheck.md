---
title: luacheck
anchor: luacheck
link: 'https://gitlab.com/pipeline-components/luacheck'
dockerimage: 'alpine:3.10'
---
The image is for running luacheck, luacheck is installed in /app/ in case you need to customize the install before usage.
For more information on luacheck checkout there [website][luacheck]

<!--more-->
## Examples

```yaml
luacheck:
  stage: linting
  image: pipelinecomponents/luacheck:latest
  script:
    - luacheck .
```

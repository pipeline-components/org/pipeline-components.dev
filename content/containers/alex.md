---
title: alex
anchor: alex
link: 'https://gitlab.com/pipeline-components/alex'
dockerimage: 'node:12.13.0-alpine'
---
The image is for running alex, alex is installed in /app/ in case you need to customize the install before usage.
The image is based on node:10.14-alpine

<!--more-->
## Examples

```yaml
alex:
  stage: linting
  image: pipelinecomponents/alex:latest
  script:
    - alex --why README.md
```

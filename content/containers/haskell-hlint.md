---
title: haskell-hlint
anchor: haskell-hlint
link: 'https://gitlab.com/pipeline-components/haskell-hlint'
dockerimage: 'alpine:3.10.3'
---
The image is for running haskell-hlint.
The image is based on alpine:3.8.

A configfile called `.hlint.yaml` is required, and a default can ben generated with:

`docker run --rm -ti pipelinecomponents/haskell-hlint:latest hlint -d > .hlint.yaml`

<!--more-->
## Examples

```yaml
haskell-hlint:
  stage: linting
  image: pipelinecomponents/haskell-hlint:latest
  script:
    - hlint .
```

To use parallel processing

```yaml
haskell-hlint:
  stage: linting
  image: pipelinecomponents/haskell-hlint:latest
  script:
    - hlint -j .
```

For more information about hlint commandline see [Hlint documentation](https://github.com/ndmitchell/hlint#installing-and-running-hlint)

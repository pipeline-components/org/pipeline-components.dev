---
title: remark-lint
anchor: remark-lint
link: 'https://gitlab.com/pipeline-components/remark-lint'
dockerimage: 'node:12.13.0-alpine'
---
The image is for running remark-lint, remark-lint is installed in /app/ in case you need to customize the install before usage.
The image is based on node:10-alpine

<!--more-->
## Examples

```yaml
remark-lint:
  stage: linting
  image: pipelinecomponents/remark-lint:latest
  script:
    - remark --no-stdout --color  --use preset-lint-recommended .
```

---
title: rubocop
anchor: rubocop
link: 'https://gitlab.com/pipeline-components/rubocop'
dockerimage: 'ruby:2.6.5-alpine3.10'
---
The image is for running rubocop, rubocop is installed in /app/ in case you need to customize the install before usage.
The image is based on ruby:2.5.3-alpine3.8

<!--more-->
## Examples

```yaml
rubocop:
  stage: linting
  image: pipelinecomponents/rubocop:latest
  script:
    - rubocop  -P -E  .

```

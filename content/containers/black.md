---
title: black
anchor: black
link: 'https://gitlab.com/pipeline-components/black'
dockerimage: 'python:3.7.5-alpine3.10'
---
The image is for running black, black is installed in /app/ in case you need to customize the install before usage.
The image is based on alpine:3.8.

<!--more-->
## Examples

```yaml
black:
  stage: linting
  image: pipelinecomponents/black:latest
  script:
    - black --check --verbose -- .
```
